import { DataService } from './../shared/services/data.service';
import { User } from './../pages/login/login';
import { AngularFireAuth } from 'angularfire2/auth';
import { PageInterface } from './app.component';
import { Component, ViewChild, OnInit } from '@angular/core';
import { Events, MenuController, Nav, Platform, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthProvider } from '../providers/auth-provider';
import { ENV } from "../config";

export interface PageInterface {
  title: string;
  name: string;
  component: any;
  icon: string;
  logsOut?: boolean;
  index?: number;
  tabName?: string;
  tabComponent?: any;
}

@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit {
  @ViewChild(Nav) nav: Nav;
  user = {} as User;

  public subscription;

  loggedInPages: PageInterface[] = [
    // { title: 'Tổng Hợp', name: 'HomePage', component: 'HomePage', icon: 'laptop'},
    { title: 'Số Lượng Hình', name: 'PhotosPage', component:'PhotosPage', icon:'photos'},
    { title: 'Các cở hình', name: 'PhotoSizePage', component:'PhotoSizePage', icon: 'list-box' },
    { title: 'Tổng Hợp Lương', name: 'SalaryPage', component: 'SalaryPage', icon: 'calculator'},
    { title: 'Đăng Xuất', name: 'LoginPage', component: 'LoginPage', icon: 'log-out', logsOut: true }
  ];

  loggedOutPages: PageInterface[] = [
    { title: 'Đăng Nhập', name: 'LoginPage', component: 'LoginPage', icon: 'log-in' },
    { title: 'Đăng kí Tài Khoản', name: 'RegisterPage', component: 'RegisterPage', icon: 'person-add' }
  ];

  rootPage: any;
  userSubscription$;

  constructor(
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    public authProv: AuthProvider,
    public toast: ToastController,
    public events: Events,
    public menu: MenuController,
    public afAuth: AngularFireAuth,
    public dataService: DataService,
  ) {
    this.initializeApp();
    this.watchForConnection();
    this.watchForDisconnection();

    // this.enableMenu(true);
    this.listenToLoginEvents();

    if (window.location.href.indexOf("localhost") < 0 && !ENV.production) {
      alert("Production and environment do not get configured correctly. Please stop using the app and contact admins");
    }

    this.authProv.onAuthStateChanged((user) => {
      if (user) {
        this.enableMenu(true);
        this.user = user;
        this.rootPage = "PhotosPage";
      } else {
        this.rootPage = "LoginPage";
      }
    })
  }

  ngOnInit() {
    
  }

  ngAfterViewInit() {
    
  }

  ngOnDestroy() {
    
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      if (this.authProv.authenticated) {
        this.user = this.authProv.currentUser;
        this.enableMenu(true);
      }

    });
  }

  watchForConnection() {
    var self = this;
    var online = navigator.onLine;

    if (online) {
      setTimeout(() => {
        console.log("we got a connection");
        console.log("Firebase: Go online");
        self.dataService.goOnline();
        self.events.publish("network:connected", true);
      })
    }
  }

  watchForDisconnection() {

  }

  openPage(page: PageInterface) {
    let params = {};

    // the nav component was found using @ViewChild(Nav)
    // setRoot on the nav to remove previous pages and only have this page
    // we wouldn't want the back button to show in this scenario
    if (page.index) {
      params = { tabIndex: page.index };
    }

    // If we are already on tabs just change the selected tab
    // don't setRoot again, this maintains the history stack of the
    // tabs even if changing them from the menu
    if (this.nav.getActiveChildNavs().length && page.index != undefined) {
      this.nav.getActiveChildNavs()[0].select(page.index);
    // Set the root of the nav with params if it's a tab index
    } else {
      this.nav.setRoot(page.name, params).catch((err: any) => {
        console.log(`Didn't set nav root: ${err}`);
      });
    }

    if (page.logsOut === true) {
      this.authProv.logout();
      this.rootPage = "LoginPage"; 
    }
  }

  listenToLoginEvents() {
    this.events.subscribe('user:login', () => {
      this.enableMenu(true);
    });

    this.events.subscribe('user:signup', () => {
      this.enableMenu(true);
    });

    this.events.subscribe('user:logout', () => {
      this.enableMenu(false);
    });
  }

  enableMenu(loggedIn: boolean) {
    this.menu.enable(loggedIn,'loggedInMenu');
    this.menu.enable(!loggedIn, 'loggedOutMenu');
  }

  isActive(page: PageInterface) {
    let childNav = this.nav.getActiveChildNavs()[0];

    if (childNav) {
      if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
        return 'primary';
      }
      return;
    }

    if (this.nav.getActive() && this.nav.getActive().name === page.name) {
      return 'primary';
    }

    return;
  }
}

import { UserProfileComponent } from './../components/user-profile/user-profile';
import { ComponentsModule } from './../components/components.module';
import { SharedModule } from './../shared/Shared.module';
import { IonicStorageModule } from '@ionic/storage';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { GLIonic2EnvConfigurationModule } from 'gl-ionic2-env-configuration';

import { FIREBASE_CONFIG } from './app.firebase.config';
import { AuthProvider } from '../providers/auth-provider';
import { UtilProvider } from './../providers/util-provider';
import { ENV } from "../config";

@NgModule({
  declarations: [
    MyApp,    
  ],
  imports: [
    GLIonic2EnvConfigurationModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(ENV.FIREBASE_CONFIG),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    IonicStorageModule.forRoot(),
    SharedModule,
    ComponentsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    UtilProvider,
  ],
  exports: [
    
  ]
})
export class AppModule {}

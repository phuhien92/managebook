import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class UserProvider {
    constructor( 
        public afAuth: AngularFireAuth
    ) {

    }

    getUid() {
        var info = window.localStorage.getItem('userInfo');
        return JSON.parse(info).uid;
    }

    createUser(user) {

    }
}
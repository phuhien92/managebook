import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AlertController } from 'ionic-angular';

import 'rxjs/add/operator/map';

@Injectable()
export class UtilProvider {
    constructor( 
        public afAuth: AngularFireAuth,
        public alertCtrl: AlertController
    ) {

    }

    alertConfirm( obj ,callback) {
        this.alertCtrl.create({
            title: obj.title,
            message: obj.message,
            buttons: [
                {
                    text: "Đúng",
                    handler: () => {
                        if (callback) callback();
                    }
                },
                {
                    text: "Không",
                    handler: () => {
                        console.log("Alert confirm: no ");
                    }
                }

            ]
        }).present();
    }
}
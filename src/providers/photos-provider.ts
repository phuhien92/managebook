import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class PhotosProvider {
    constructor( 
        public afAuth: AngularFireAuth
    ) {

    }

    getUid() {
        var info = window.localStorage.getItem('userInfo');
        return JSON.parse(info).uid;
    }

    createUser(user) {
        let uid = this.getUid();
    }
}
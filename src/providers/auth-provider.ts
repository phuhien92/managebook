import { User } from './../models/user';
import { AngularFireDatabase } from 'angularfire2/database';
import { Events, ToastController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';

import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Observable } from 'rxjs/Observable';
import "rxjs/add/observable/of";
import "rxjs/add/operator/switchMap";

import { todayToValue } from "../helpers";

@Injectable()
export class AuthProvider {
    
    authState: any = null;
    HAS_LOGGED_IN = 'hasLoggedIn';
    user: BehaviorSubject<User> = new BehaviorSubject(null);

    constructor(
        public afAuth: AngularFireAuth,
        public events: Events,
        public storage: Storage,
        public db: AngularFireDatabase,
        public toast: ToastController
    ) {
        this.afAuth.authState.subscribe(auth => {
            this.authState = auth;
        })
    }
    
    get authenticated():boolean {
        return this.authState !== null;
    }

    get currentUser(): any {
        return this.authenticated ? this.authState : null;
    }

    get currentUserObservable(): any {
        return this.afAuth.authState;
    }

    get currentUserId(): string {
        return this.authenticated ? this.authState.uid : '';
    }

    get currentUserDisplayName(): string {
        if (!this.authState) {
            return "Guest";
        } else {
            return this.authState['displayName'] || 'User without a name';
        }
    }

    onAuthStateChanged(callback) {
        return this.afAuth.auth.onAuthStateChanged(callback);
    }

    login(user) {
        return this.afAuth.auth.signInWithEmailAndPassword(
            user.email,
            user.password
        ).then(user => {
            this.authState = user;
            this.updateUser();
            this.events.publish("user:login");
        })
    }    

    logout() {
        this.afAuth.auth.signOut();
        this.events.publish("user:logout");
        this.toast.create({
            message: "Bạn đã đăng xuất khỏi tài khoản hiện tại.",
            duration: 3000
        }).present();
    }

    sendResetPassword(email: string) {
        return this.afAuth.auth.sendPasswordResetEmail(email);
    }

    register(user) {
        return this.afAuth.auth.createUserWithEmailAndPassword(
            user.email,
            user.password
        )
        .then( user => {
            this.authState = user;
            this.events.publish("user:signup");
        })
        .catch(error => console.log("Register Code Error: ", error));
    }

    // Helper functions ...
    private updateUser(option?) {
        const createdAt = option && option.created_at ? option.created_at : todayToValue();
        const path = `users/${this.currentUserId}`;
        const data = {
            email: this.authState.email,
            displayName: this.authState.displayName,
            created_at: createdAt,
            updated_at: todayToValue()
        }

        try {
            this.db.object(path).update(data);
        } catch(error) {
            console.error("updateUser: error :", error);
        }
    }
}

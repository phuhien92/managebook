export class PhotoSize {
    $key: string;
    sizeName: string;
    price: number;
    score: boolean  = false;
    salary: boolean = false;
    unit: string;
    created_at: number;
    updated_at: number;
    disabled: boolean = false;
}

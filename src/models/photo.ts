import { PhotoSize } from './photosize';

export class Photo {
    $key?: string;
    quantity: number;
    score: number;
    salary: number;
    created_at: number;
    updated_at: number;
    customer: string;
    user: any;
    uid: string;
    photosize: any;
    photosizeid: string;
    approved?: boolean;
    reject?: boolean;
    pending?: boolean;
}

export class Product {
    id: number;
    photosize: PhotoSize;
    quantity: number;
}
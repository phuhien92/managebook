import { todayToValue } from '../helpers';

export interface Roles {
    employee: boolean;
    employer?: boolean;
    admin?: boolean;
}

export class IUser {
    uid: string;
    email: string;
    created_at: number;
    update_at: number;
    disabled: false;
    displayName: string;
}
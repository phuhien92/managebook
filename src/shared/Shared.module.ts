import { DataService } from './services/data.service';
import { ValuesPipe } from './pipes/values.pipe';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

// components

@NgModule({
  declarations: [
    ValuesPipe,
    
  ],
  exports: [
    CommonModule,
    ValuesPipe
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    
  ],
  providers: [
    DataService
  ]
})

export class SharedModule {};

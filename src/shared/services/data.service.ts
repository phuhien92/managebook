import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

// declare var firebase: any;

@Injectable()
export class DataService {
    // databaseRef: any        = firebase.database();
    // statisticsRef: any      = firebase.database().ref('statistics');
    // storageRef: any         = firebase.storage().ref();
    // connectionRef: any      = firebase.database().ref('.info/connected');
    // photosRef: any          = firebase.database().ref('photos');

    connected: boolean = false;

    constructor(
        public db: AngularFireDatabase
    ) {
        var self = this;

        try {
            self.checkFirebaseConnection();

        } catch (error) {

        }
    }

    checkFirebaseConnection() {
        try {
            var self = this;
        } catch (error) {
            self.connected = false;
        }
    }

    goOffline() {
        return false;
    }

    goOnline() {
        return true;
    }

    getISODate(datetime: Date = new Date()):string {
        const today = new Date();
        const date  = new Date( today.getFullYear() + "/" + (today.getMonth() + 1) + "/"+ today.getDate() );
        
        return date.toISOString();
    }

    getTodayDate(): string {
        return this.getISODate();
    }

    getTomorrowDate(): string {
        const today = new Date();
        const tomorrow = new Date();

        tomorrow.setDate(today.getDate() + 1);

        const datetime = new Date( tomorrow.getFullYear() + "/" + (tomorrow.getMonth() +1) + "/" + tomorrow.getDate() );
        return datetime.toISOString();
    }
}
import { PhotoSizeService } from './../../services/photosize.service';
import { FormGroup } from '@angular/forms';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'product',
  templateUrl: 'product.html'
})
export class ProductComponent {
  public ps_list: any;

  @Input('group')
  public productForm: FormGroup

  constructor(
    public psService: PhotoSizeService,
  ) {
    this.getPhotoSizeList();
  }

  async getPhotoSizeList() {
    this.psService.getPhotoSizeList().subscribe(data => {
      this.ps_list = data.reverse();
      console.log("Photo size list Product: ",data);
    });
  }

}

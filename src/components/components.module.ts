import { SharedModule } from './../shared/Shared.module';
import { UserService } from './../services/user.service';
import { PhotoService } from './../services/photos.service';
import { PhotosFilterComponent } from './photos-filter/photos-filter';
import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { UserProfileComponent } from './user-profile/user-profile';

@NgModule({
  declarations: [
    PhotosFilterComponent,
    UserProfileComponent
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
  ],
  exports: [
    PhotosFilterComponent,
    UserProfileComponent
  ],
  providers: [
    PhotoService,
    UserService
  ]
})
export class ComponentsModule {

}
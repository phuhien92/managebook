import { UserData } from './../../models/user';
import { AuthProvider } from './../../providers/auth-provider';
import { Component, Input } from '@angular/core';
import * as md5 from "md5";

@Component({
  selector: 'user-profile',
  templateUrl: 'user-profile.html'
})
export class UserProfileComponent {
  @Input() currentUser: any;

  public DEFAULT_SIZE: number = 50;

  constructor() {
    console.log(this.currentUser)
  }

  ionViewWillEnter() {
    
  }

  fetchAvatar(email: string = "", defaultStyle: string = "retro", size: number = this.DEFAULT_SIZE) {
    return `https://secure.gravatar.com/avatar/
    ${md5(email)}?
    d=${defaultStyle}&
    s=${size}`.replace(/\s/g, "");
  }
}

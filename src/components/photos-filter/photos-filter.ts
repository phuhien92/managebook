import { DataService } from './../../shared/services/data.service';
import { UserService } from './../../services/user.service';
import { Events } from 'ionic-angular';
import { Validators } from '@angular/forms';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'photos-filter',
  templateUrl: 'photos-filter.html'
})
export class PhotosFilterComponent {
  @Input() event: string ;
  public gen_filter: any;
  public filterForm: FormGroup;
  public people: any;

  constructor(
    public fb: FormBuilder,
    public events: Events,
    public uService: UserService,
    public dataService: DataService
  ) {
    // let today = new Date();
    // let day   = today.getDate();
    // let month = today.getMonth() + 1;
    // let year  = today.getFullYear();

    // let tomorrow = new Date();
    // tomorrow.setDate(today.getDate() + 1);

    // let today_startAt = new Date( year + "/" + month + "/"+day);
    // let today_endAt   = new Date( tomorrow.getFullYear() + "/" + (tomorrow.getMonth() +1) + "/" + tomorrow.getDate() );
    let today_startAt = this.dataService.getTodayDate();
    let today_endAt   = this.dataService.getTomorrowDate();

    this.getEmployeeList();

    this.filterForm = this.fb.group({
      uid:      [ ""],
      startAt:  [ today_startAt, Validators.required ],
      endAt:    [ today_endAt, Validators.required]
    })

    this.submit();
  }

  ngOnInit() {
    
  }

  async getEmployeeList() {
    this.uService.getUserList().subscribe( data => {
      this.people = data.reverse();
      console.log("Employee: ", data);
    })
  }

  submit() {
    if (this.filterForm.valid) {
      this.events.publish( this.event , { data: this.filterForm.value } );
    }
  }
}

import { ROOTPAGE } from './../../helpers';
import { AuthProvider } from './../../providers/auth-provider';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ToastController, LoadingController } from 'ionic-angular';
import { NgForm } from '@angular/forms';

// models
import { IUser } from './../../models/user.model';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  user      = {} as IUser;
  login     = { email: "", password: ""};
  submitted = false;
  rootPage  = 'PhotosPage';
  hasLoggedIn: boolean = false;

  constructor(
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public toast: ToastController,
    public authProv: AuthProvider,
    public navCtrl: NavController, 
    public navParams: NavParams,
    private events: Events,

  ) {
    
  }

  ionViewDidLoad() {
    this.hasLoggedIn = this.authProv.authenticated;
  }

  ionViewWillEnter() {
    if (this.hasLoggedIn) {
      this.navCtrl.setRoot(ROOTPAGE)
    }
  }

  go(path) {
    this.navCtrl.push(path);
  }

  goRegister(): void {
    this.navCtrl.setRoot('RegisterPage');
  }

  onLogin(form: NgForm): void {
    let self = this;

    try {
      self.submitted = true;

      if (form.valid) {

        let loader = self.loadingCtrl.create({
          content: "Bạn đang đăng nhập. Xin chờ giây lát ...",
          dismissOnPageChange: true 
        })

        loader.present();

        self.authProv.login(this.login)
        .then( data => {
          // console.info("Login Component:", data);
          this.navCtrl.setRoot(this.rootPage);
          this.events.publish('user:login')
          // setTimeout( _ => {
          //   window.location.reload();
          // }, 500)
        })
        .catch( (error) => {
          let errorMessage  = error.message;
          loader.dismiss().then(() => {
            self.toastCtrl.create({
              message: errorMessage,
              duration: 4000,
              position: 'top'
            }).present();
          })

        })
      }
      
    } catch(e) {
      console.error("Login E:", e);
    }
  }
}

import { AuthProvider } from './../../providers/auth-provider';
import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    public navCtrl: NavController,
    public authProv: AuthProvider
  ) {

  } 

  ionViewWillLoad() {
    // this.afAuth.authState.subscribe( data => {
    //   console.log(data);
    //   if (data && data.email && data.uid ) {
    //     this.toast.create({
    //       message: 'Chao mung '+data.email,
    //       duration: 3000
    //     }).present();
    //   } else {
    //     this.toast.create({
    //       message: "Ban khong co quyen dang nhap",
    //       duration: 3000
    //     }).present();

    //     this.navCtrl.setRoot("LoginPage");
    //   }
    // })
  }

  signout() {
    this.authProv.logout()
    // .then(data => {
    //   console.info("Signout", data);

    //   this.toast.create({
    //     message: "You are signed out",
    //     duration: 3000
    //   }).present();

    //   this.navCtrl.setRoot("LoginPage");
    // })
  }
}

import { PhotoSizeService } from './../../services/photosize.service';
import { PhotoSizeAddAndUpdatePage } from './photo-size-add-and-update';
import { PhotoService } from './../../services/photos.service';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
  declarations: [
    PhotoSizeAddAndUpdatePage,
  ],
  imports: [
    IonicPageModule.forChild(PhotoSizeAddAndUpdatePage),
  ],
  exports: [
    PhotoSizeAddAndUpdatePage
  ],
  providers: [
    PhotoSizeService
  ]
})
export class PhotoSizeAddAndUpdateModule {}
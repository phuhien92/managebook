import { PhotoSizeService } from './../../services/photosize.service';
import { PhotoSize } from './../../models';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Component, OnDestroy, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage({
  defaultHistory: ['PhotoSizePage']
})
@Component({
  selector: 'page-photo-size-add-and-update',
  templateUrl: 'photo-size-add-and-update.html',
})
export class PhotoSizeAddAndUpdatePage implements OnInit, OnDestroy { 
  public photoList: any;
  public form: FormGroup;
  public photosize = {} as PhotoSize;
  public action: string = "POST";
  public units = ["điểm","đồng (VNĐ)"];
  public isSizeNameUnique: boolean = true;

  constructor(
    public psService: PhotoSizeService,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public view: ViewController,
    private fb: FormBuilder
  ) {
    if ( this.navParams.get("photosize_list")) {
      this.photoList = this.navParams.get("photosize_list");
    }

    if (this.navParams.get("action") ){
      this.action = this.navParams.get("action");
    }

    if ( this.navParams.get("photosize") ) {
      this.photosize = this.navParams.get("photosize");
    }

    this.createForm( this.photosize );
  }

  ngOnInit() {
    
  }

  ngOnDestroy() {

  }

  createForm( photosize: PhotoSize ) {
    if ( this.action === "POST" ) {
      photosize.created_at = this.psService.getCurrentTime();
      photosize.updated_at = this.psService.getCurrentTime();
    }

    this.form = this.fb.group({
      sizeName: [ photosize.sizeName, Validators.required ],
      price: [ photosize.price, Validators.required ],
      unit: [ photosize.unit, Validators.required ],
      created_at: [ photosize.created_at, Validators.required ],
      updated_at: [ photosize.updated_at, Validators.required ],
      disabled: [ false, Validators.required ]
    })
  }

  closeModal(data = {}) {
    if (data) {
      this.view.dismiss(data);
    } else {
      this.view.dismiss();
    }
  }

  submit() {
    let valid = this.validateSizenameUnique();

    if ( !this.form.valid ) return;

    if ( this.action === "POST" ) {

      if (valid) {
        this.psService.createPhotosizeData( this.form.value )
        .then( _ => {
          this.closeModal({
            action: this.action,
            status: 200
          })
        });
      } 
      console.log( this.form );

    } else if ( this.action === "PUT" ) {
      this.psService.updatePhotosizeData( this.photosize.$key ,this.form.value )
      .then( _ => {
        this.closeModal({
          action: this.action,
          status: 200
        });
      }, (error) => {
        console.log("Update error : photosize :", error);
      })
    }
  }

  validateSizenameUnique() {
    let exist = false;
    this.photoList.map( photo => {
      if ( photo.sizeName === this.form.value.sizeName ) {
        exist = true;
      }
    })

    this.isSizeNameUnique = !exist;

    return !exist;
  }

}

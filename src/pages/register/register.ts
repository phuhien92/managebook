import { ENV } from './../../config/config.dev';
import { AuthProvider } from './../../providers/auth-provider';
import { AngularFireDatabase } from 'angularfire2/database';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { NgForm } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  user = {
    email: "",
    password: "",
    displayName: "",
    adminPassword: ""
  }
  submitted = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public authProv: AuthProvider,
    private afAuth: AngularFireAuth,
    private db: AngularFireDatabase,

  ) {
  }

  ionViewDidLoad() {}

  register(form: NgForm) {
    try {
      this.submitted = true;
      
      if ( form.valid && this.user.adminPassword === ENV.ADMIN_PASSWORD) {
        this.authProv.register(this.user);
      }
    }
    catch(e) {
      console.error("Register Error",e);
    }
  }

  goLogin() {
    this.navCtrl.setRoot('LoginPage');
  }
}

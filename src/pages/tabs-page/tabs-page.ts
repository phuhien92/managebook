import { NavParams } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
    templateUrl: "tabs-page.html"
})

export class TabsPage {
    tab1Root: any = "HomePage";
    tab2Root: any = "PhotosPage";
    tab3Root: any = "PhotoSizePage";
    tab4Root: any = "SalaryPage";
    
    mySelectedIndex: number;

    constructor( navParams: NavParams ) {
        this.mySelectedIndex = navParams.data.tabIndex || 0;
    }
}
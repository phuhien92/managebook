import { AuthProvider } from './../../providers/auth-provider';
import { NgForm } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage,NavController, NavParams, ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html',
})
export class ResetPasswordPage {
  user = {
    email: ""
  };
  submitted = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public authProv: AuthProvider,
    public toastCtrl: ToastController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetPasswordPage');
  }

  onSubmit(email: string, form: NgForm) {
    var self = this;
    self.submitted = true;

    if ( form.valid ) {
      self.authProv.sendResetPassword( email ).then( _ => {
        self.toastCtrl.create({
          message: "Email đổi mật khẩu đã gữi. Bạn có thể kiểm tra email.",
          duration: 5000
        }).present();

        self.navCtrl.setRoot("LoginPage");
      })
    }
  } 

}

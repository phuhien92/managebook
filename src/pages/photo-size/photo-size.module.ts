import { PhotosizeEffect } from './../../store/effects/photosize.effects';
import { PhotoSizeService } from './../../services/photosize.service';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhotoSizePage } from './photo-size';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { default as reducer } from '../../store/app.store';


@NgModule({
  declarations: [
    PhotoSizePage,
  ],
  imports: [
    IonicPageModule.forChild(PhotoSizePage),

    // store
    // StoreModule.provideStore( reducer ),

    // ngrx effects
    // EffectsModule.run( PhotosizeEffect )
  ],
  exports: [
    PhotoSizePage
  ],
  providers: [
      PhotoSizeService
  ]
})
export class PhotoSizePageModule {}
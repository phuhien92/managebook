import { PhotoSize } from './../../models/photosize';
import { UtilProvider } from './../../providers/util-provider';
import { PhotoSizeService } from './../../services/photosize.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-photo-size',
  templateUrl: 'photo-size.html',
})
export class PhotoSizePage implements OnInit, OnDestroy {
  photosize_list: PhotoSize[];
  photoSizeDB: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public psService: PhotoSizeService,
    public utilProv: UtilProvider,
    public modalCtrl: ModalController,
    public toast: ToastController
  ) {
    this.getPhotoSizeList();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PhotoSizePage');
  }

  ngOnInit() {}

  ngOnDestroy() {
    if ( this.photoSizeDB ) {
      this.photoSizeDB.unsubscribe();
    }
  }

  async getPhotoSizeList() {
    this.photoSizeDB = await this.psService.getPhotoSizeList().subscribe(data => {
      this.photosize_list = data;
    });
  }

  remove(photosize) {
    this.utilProv.alertConfirm({
      title: "Hỏi",
      message: "Bạn muốn xoá kích cở hình "+photosize.sizeName + " phải không ?"
    }, () => {
      this.psService.deletePhotosize(photosize.$key).then( _ => {
        console.log("Delete "+photosize.sizeName);
      });
    })
  }

  disable(photosize) {
    this.utilProv.alertConfirm({
      
      title: "Hỏi",
      message: "Bạn muốn xoá kích cở hình "+photosize.sizeName + " phải không ?"
    }, () => {
      this.psService.disablePhotosize(photosize.$key).then( _ => {
        console.log("Delete-disabled "+photosize.sizeName);
      });
    })
  }

  openModal(photosize) {
    let modal;
    let option = {
      enableBackdropDismiss: false
    };

    if (photosize) {
      modal = this.modalCtrl.create("PhotoSizeAddAndUpdatePage", { photosize: photosize, photosize_list: this.photosize_list , action: "PUT" }, option)
    } else {
      modal = this.modalCtrl.create("PhotoSizeAddAndUpdatePage", { action: "POST", photosize_list: this.photosize_list } , option)
    }
    
    modal.onDidDismiss( data => {
      this.toast.create({
        message: "Dữ liệu đã được lưu thành công",
        duration: 3000
      })
    })

    modal.present();
  }

}

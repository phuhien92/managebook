import { ProductComponent } from './../../components/product/product';
import { PhotoService } from './../../services/photos.service';
import { UserService } from './../../services/user.service';
import { PhotoSizeService } from './../../services/photosize.service';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhotoActionsModalPage } from './photo-actions-modal';

@NgModule({
  declarations: [
    PhotoActionsModalPage,
    ProductComponent
  ],
  imports: [
    IonicPageModule.forChild(PhotoActionsModalPage),
    
  ],
  exports: [
    
  ],
  providers: [
      PhotoSizeService,
      UserService,
      PhotoService
  ]
})
export class PhotoActionsModalPageModule {}
import { PhotoService } from './../../services/photos.service';
import { UserService } from './../../services/user.service';
import { PhotoSizeService } from './../../services/photosize.service';
import { Photo, Product } from './../../models/photo';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from "@angular/forms";

@IonicPage({
  defaultHistory: ['PhotosPage']
})
@Component({
  selector: 'page-photo-actions-modal',
  templateUrl: 'photo-actions-modal.html',
})
export class PhotoActionsModalPage implements OnInit, OnDestroy {
  form: FormGroup;
  photo = {} as Photo;
  people: any[];
  ps_list: any[];
  numbers = [];
  today: number;
  action: string = "CREATE";
  public units = ["điểm","đồng (VNĐ)"];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public pService: PhotoService,
    public psService: PhotoSizeService,
    public uService: UserService,
    private view: ViewController,
    private fb: FormBuilder,
  ) {
    
  }

  ionViewDidLoad() {}

  ngOnDestroy() {

  }

  ngOnInit() {
    let today = new Date();

    this.today = today.valueOf();
    this.getPhotoSizeList();
    this.getEmployeeList();

    if (this.navParams.get("photo")) {
      this.photo = this.navParams.get("photo");
      console.log(this.photo)
    } 

    if (this.navParams.get("action")) {
      this.action = this.navParams.get("action");
    }

    // console.log("Action: ", this.action)
    this.createForm(this.photo);
  }

  createForm(photo: Photo) {

    this.form = this.fb.group({
      quantity:   [ photo.quantity, Validators.required ],
      created_at: [ (photo.created_at ? photo.created_at : this.today ), Validators.required ],
      updated_at: [ this.today, Validators.required ], 
      customer:   [ (photo.customer ? photo.customer : "" ) ],
      user:       [ (photo.uid ? photo.uid : "" ), Validators.required],
      photosize:  [ (photo.photosizeid ? photo.photosizeid : ""), Validators.required],
      products: this.fb.array([
        this.buildProduct(),
      ])
    })

    // console.log(this.form)

  }

  async getPhotoSizeList() {
    this.psService.getPhotoSizeList().subscribe(data => {
      let result = data.filter( ps => ps.disabled == false )

      this.ps_list = result.reverse();
      console.log("Photo size list: ",this.ps_list);
    });
  }

  async getEmployeeList() {
    this.uService.getUserList().subscribe( data => {
      this.people = data.reverse();
      console.log("Employee: ", data);
    });
  }

  submit() {
    // if ( !this.form.valid ) return;

    if (this.action === "CREATE") {
      // this.pService.createPhotoData(this.form.value).then( _ => {
      //   console.info("Create photo suceeded");

      //   this.closeModal("success");

      // }, (error) => {
      //   console.error("Create Photo: ", error);
      // })

      this.form.value.products.map(product => {

        let user      = this.people.filter( person => person.uid === this.form.value.user );
        let photosize = this.ps_list.filter( ps => ps.$key === product.photosize );
        let score     = this.scoreCount(  photosize[0].unit ,product.quantity, photosize[0].price );
        let salary    = this.salaryCount( photosize[0].unit ,product.quantity, photosize[0].price );

        console.log( score )
        console.log( salary)
        console.log(photosize)

        let data = {
          quantity: parseInt(product.quantity),
          customer: this.form.value.customer,
          created_at: this.form.value.created_at,
          updated_at: this.form.value.updated_at,
          user: user[0],
          uid: this.form.value.user,
          photosize: photosize[0],
          photosizeid: product.photosize,
          score: score,
          salary: salary
        };

        this.pService.createPhotoData(data);
      })

      this.navCtrl.pop();
      return false;
    } else {

      let user      = this.people.filter( person => person.uid === this.form.value.user );
      let photosize = this.ps_list.filter( ps => ps.$key === this.form.value.photosize );
      let score     = this.scoreCount( photosize[0].unit ,this.form.value.quantity, photosize[0].price );
      let salary    = this.salaryCount( photosize[0].unit ,this.form.value.quantity, photosize[0].price );
      let data      = {
        quantity: parseInt(this.form.value.quantity),
        customer: this.form.value.customer,
        created_at: this.form.value.created_at,
        updated_at: this.form.value.updated_at,
        user: user[0],
        uid: this.form.value.user,
        photosize: photosize[0],
        photosizeid: this.form.value.photosize,
        score: score,
        salary: salary
      };

      this.pService.updatePhotoData(this.photo.$key, data ).then( _ => {
        console.info("Update photo suceeded");

        this.closeModal("success");

      }, (error) => {
        console.error("Create Photo: ", error);
      })
    }

    return false;
  }

  scoreCount( unit, quantity, price ): number {
    return ( unit === this.units[0] ) ? (quantity*price) : 0;
  }

  salaryCount( unit, quantity, price ): number {
    return ( unit === this.units[1] ) ? (quantity*price) : 0;
  }

  closeModal(data) {
    if (data) {
      this.view.dismiss(data);
    } else {
      this.view.dismiss();
    }
  }

  add_new_product() {
    try {
      const control = <FormArray>this.form.controls['products'];
      control.push(this.buildProduct());
    } catch(error) {
      console.log(error)
    }

    return false;
  }

  remove_product(i:number) {
    const control = <FormArray>this.form.controls['products'];
    return control.removeAt(i);
  }

  buildProduct() {
    let product = new Product();

    return new FormGroup({
        photosize: new FormControl( product.photosize, Validators.required ),
        quantity: new FormControl(product.quantity)
    })
  }
 }

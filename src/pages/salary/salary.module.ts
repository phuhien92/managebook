import { SharedModule } from './../../shared/Shared.module';
import { PhotoSizeService } from './../../services/photosize.service';
import { ComponentsModule } from './../../components/components.module';
import { UserService } from './../../services/user.service';
import { PhotoService } from './../../services/photos.service';
import { SalaryPage } from './salary';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
  declarations: [
    SalaryPage
  ],
  imports: [
    IonicPageModule.forChild(SalaryPage),
    ComponentsModule,
    SharedModule
  ],
  exports: [
    SalaryPage, 
  ],
  providers: [
    PhotoService,
    UserService,
    PhotoSizeService,
    
  ]
})
export class SalaryPageModule {

}

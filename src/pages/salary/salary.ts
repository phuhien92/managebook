import { AngularFireDatabase } from 'angularfire2/database';
import { PhotoSizeService } from './../../services/photosize.service';
import { UserService } from './../../services/user.service';
import { PhotoService } from './../../services/photos.service';
import { CHART_BACKGROUND, CHART_BORDER } from './../../shared';
import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { Chart } from 'chart.js';
import 'rxjs/add/observable/forkJoin'

@IonicPage()
@Component({

  selector: 'page-salary',
  templateUrl: 'salary.html',
  
})
export class SalaryPage implements OnInit {
  @ViewChild('barCanvas') barCanvas;
  barChart: any;
  data: any;
  startAt: number;
  endAt: number;
  photos: any[];
  users: any = {};
  photosizeList: any = {};
  statistics: any = {
    total: 0,
    score: 0,
    salary: 0,
    category: {}
  };
  public firebaseConnectionAttempts: number = 0;
  public start: number;
  public pageSize: number = 3;
  public loading: boolean = true;
  public internetConnected: boolean = true;
  public graphData    = [];
  public graphLabels  = [];
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private events: Events,
    public photoService:PhotoService,
    public userService: UserService,
    public photosizeService: PhotoSizeService,
    public db: AngularFireDatabase,
  ) {
    this.events.subscribe("salary:filter", result => {
      this.data = result.data;
      let startAt = new Date(this.data.startAt).valueOf();
      let endAt   = new Date(this.data.endAt).valueOf();

      this.getPhotosList({
        orderByChild: "created_at",
        startAt: { key: "created_at", value: startAt },
        endAt: { key: "updated_at", value: endAt }
      }, this.data.uid);
    
    })
  }

  ionViewDidLoad() {}

  ngOnInit() {
    var self = this;
    self.events.subscribe("network:connected", self.networkConnected);
    
  }

  checkFirebase() {
    let self = this;

    // if ( !self.dataService.isFirebaseConnected()) {
    //   setTimeout(function () {
    //     console.log('Retry : ' + self.firebaseConnectionAttempts);
    //     self.firebaseConnectionAttempts++;
    //     if (self.firebaseConnectionAttempts < 5) {
    //       self.checkFirebase();
    //     } else {
    //       // self.internetConnected = false;
    //       // self.dataService.goOffline();
    //       // self.loadSqliteThreads();
    //     }
    //   }, 1000);
    // }
  }

  public networkConnected = (connection) => {
    var self = this;
    self.internetConnected = connection[0];
    console.log('NetworkConnected event: ' + self.internetConnected);

    if (self.internetConnected) {
      self.photos = [];
      self.loadPhotos(true);
    } else {
      // self.notify('Connection lost. Working offline..');
      // // save current threads..
      // setTimeout(function () {
      //   console.log(self.threads.length);
      //   self.sqliteService.saveThreads(self.threads);
      //   self.loadSqliteThreads();
      // }, 1000);
    }
  }

  loadPhotos(fromStart: boolean) {
    // var self = this;

    // if (fromStart) {
    //   self.loading = true;
    //   self.photos  = [];
      
    //   self.getPhotos();
    // }
  }

  async getPhotosList(option, uid = "") {
    return await this.photoService.getPhotosList(option)
    .subscribe( photos => {
      // console.log("Photos: ", photos);
      this.photos = uid != "" ? photos.filter( photo => photo.uid == uid):photos;
      this.photoCount( this.photos );
      this.scoreCount( this.photos );
      this.salaryCount( this.photos );
      this.photoSortByCategory( this.photos );
    })
  }

  photoSortByCategory(photos) {
    this.statistics.category = this.photoService.photoSortByCategory(photos);

    this.graphData = [];
    this.graphLabels = [];

    Object.keys(this.statistics.category).map( key => {
      this.graphLabels.push( this.statistics.category[key].sizeName );
      this.graphData.push( this.statistics.category[key].quantity );
    });

    this.barGenerator( this.graphLabels, this.graphData, " Số lượng: " )
  }

  photoCount(data: any) {
    this.statistics.total = this.photoService.photoCount(data);
  }

  scoreCount(photos: any) {    
    this.statistics.score = this.photoService.scoreCount(photos);
  }

  salaryCount(photos) {
    this.statistics.salary = this.photoService.salaryCount(photos);
  }

  barGenerator( labels, data, dataset_title ) {
    let backGround  = CHART_BACKGROUND.slice(0, data.length);
    let chartBorder = CHART_BORDER.slice(0, data.length);
    this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: 'bar',
      data: {
        labels: labels,
          datasets: [{
              label: dataset_title,
              data: data,
              backgroundColor: backGround,
              borderColor: chartBorder,
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
      }
    })
  }
}

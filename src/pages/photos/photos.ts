import { UtilProvider } from './../../providers/util-provider';
import { Photo } from './../../models/photo';
import { PhotoService } from './../../services/photos.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController, Events } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-photos',
  templateUrl: 'photos.html',
})
export class PhotosPage {
  photos: Photo[];
  total_photos: number;
  data: any;

  public photoSubscription;

  constructor(
    public utilProv: UtilProvider,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public photoService: PhotoService,
    public psService: PhotoService,
    public toast: ToastController,
    private modal: ModalController,
    public events: Events
  ) {
    this.events.subscribe("photos:filter", result => {
      this.data = result.data;
      let startAt = new Date(this.data.startAt).valueOf();
      let endAt   = new Date(this.data.endAt).valueOf();

      // console.log("photos subscribe:", result);
      this.getPhotosList({
        orderByChild: "created_at",
        startAt:      { key: "created_at", value: startAt },
        endAt:        { key: "updated_at", value: endAt },
      }, this.data.uid);
    })
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    if (this.photoSubscription) {
      this.photoSubscription.unsubscribe();
    }
  }

  ionViewDidLoad() {}

  async getPhotosList( option, uid = "" ) {
    this.photoSubscription =  this.photoService.getPhotosList(option)
    .subscribe(photos => {
      this.photos = uid != "" ? photos.filter( photo => photo.uid == uid):photos;
      this.total_photos = this.photoService.photoCount(photos);
    });
  }

  remove(photo) {
    this.utilProv.alertConfirm({
      title: "Hỏi",
      message: "Bạn muốn xoá sản phẩm này phải không ?"
    }, () => {
      
    })
  }

  openModal(photo) {
    let modal;
    let option = {
      enableBackdropDismiss: false
    };

    if (photo) {
      modal = this.modal.create("PhotoActionsModalPage", { photo: photo, action: "PUT" }, option)
    } else {
      modal = this.modal.create("PhotoActionsModalPage", {} , option)
    }

    modal.onDidDismiss( data => {
      this.toast.create({
        message: "Sản phẩm đã được lưu thành công",
        duration: 3000
      })
    })

    modal.present();
  }

  go(path) {
    this.navCtrl.push(path);
  }
}


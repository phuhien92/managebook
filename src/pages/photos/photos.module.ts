import { ComponentsModule } from './../../components/components.module';
import { PhotoService } from './../../services/photos.service';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhotosPage } from './photos';

@NgModule({
  declarations: [
    PhotosPage,
    
  ],
  imports: [
    IonicPageModule.forChild(PhotosPage),
    ComponentsModule
  ],
  exports: [
    PhotosPage
  ],
  providers: [
    PhotoService
  ]
})
export class PhotosPageModule {}
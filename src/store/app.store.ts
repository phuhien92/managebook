import { photosizes } from './reducers';
import { PhotoSize } from './../models/photosize';
import { combineReducers } from '@ngrx/store';
import { compose } from '@ngrx/core/compose';

export interface AppStore {
    photosizes: PhotoSize[]
}

export default compose(combineReducers) ({
    photosizes: photosizes 
})
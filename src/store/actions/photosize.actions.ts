import { CustomAction } from './../../models/custom-action';
import { PhotoSize } from './../../models/photosize';
import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store'; 

@Injectable()
export class PhotosizeActions {

    constructor(payload: any = null) {

    }

    static LOAD_PHOTOSIZES = 'LOAD_PHOTOSIZES';
    loadPhotosizes(): Action {
        return {
            type: PhotosizeActions.LOAD_PHOTOSIZES
        }
    }

    static LOAD_PHOTOSIZES_SUCCESS = 'LOAD_PHOTOSIZES_SUCCESS';
    loadPhotosizesSuccess( photosizes: PhotoSize[] ): CustomAction {
        return {
            type: PhotosizeActions.LOAD_PHOTOSIZES_SUCCESS,
            payload: photosizes
        };
    }
}

import { Observable } from 'rxjs/Observable';
import { PhotoSize } from './../../models/photosize';
import { PhotoSizeService } from './../../services/photosize.service';
import { PhotosizeActions } from './../actions/photosize.actions';
import { Actions, Effect } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';

import 'rxjs/add/operator/switchMap';

@Injectable()
export class PhotosizeEffect {
    constructor(
        private actions$: Actions,
        private photosizeActions: PhotosizeActions,
        private photosizeService: PhotoSizeService
    ) {

    }

    @Effect()
    loadPhotosize$ = this.actions$
    .ofType( PhotosizeActions.LOAD_PHOTOSIZES )
    .switchMap( (action: Action) => this.photosizeService.getPhotoSizeList() )
    .map( ( photosizes: PhotoSize[] ) => this.photosizeActions.loadPhotosizesSuccess(photosizes) );
}
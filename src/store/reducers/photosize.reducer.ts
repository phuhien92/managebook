import { CustomAction } from './../../models/custom-action';
import { PhotosizeActions } from './../actions/photosize.actions';
import { PhotoSize } from './../../models/photosize';
import { Observable } from 'rxjs/Observable';
import '../../rxjs-extensions';
import {Action} from '@ngrx/store'; 

export const photosizes = ( state: any = [], action: CustomAction): PhotoSize[] => {
    switch ( action.type ) {
        case PhotosizeActions.LOAD_PHOTOSIZES_SUCCESS:
            return action.payload
        default:
            return state;
    }
}

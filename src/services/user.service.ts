import { AngularFireAuth } from 'angularfire2/auth';
import { Events } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class UserService {
    
    constructor(
        public db: AngularFireDatabase,
        public events: Events
    ) {

    }

    getUserList() {
        return this.db.list('/users');
    }

    fetchUser(key) {
        return this.db.list("/users/"+key);
    }

    deleteUser(key: string) {
        return this.db.object('/users/'+key).remove();
    }

    getUser(key) {
        return this.db.object("/users/"+key);
    }

    logout(): void {
        this.events.publish("user:logout");
    }
}
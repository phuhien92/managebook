import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PhotoSize } from '../models/PhotoSize';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class PhotoSizeService {

    constructor(
        public db: AngularFireDatabase
    ) {

    }

    getPhotoSizeList(): Observable<PhotoSize[]> {
        return this.db.list('/photosize');
    }

    deletePhotosize(key: string) {
        return this.db.object('/photosize/'+key).remove();
    }

    disablePhotosize(key: string) {
        return this.db.object('/photosize/'+key).update({
            "disabled": true
        })
    }

    createPhotosizeData(data) {
        return this.db.list('/photosize').push(data);
    }

    updatePhotosizeData(key: string, data: any = {}) {
        return this.db.object('/photosize/'+key).update(data);
    }

    getCurrentTime(): number {
        let date = new Date();
        let today= date.valueOf();

        return today.valueOf();
    }

    getPhotosize(key) {
        return this.db.object("/photosize/"+key);
    }
}
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class PhotoService {

    constructor(
        public db: AngularFireDatabase
    ) {}

    getPhotosList(option = { orderByChild: "created_at" } ) {
        
        return this.db.list('/photos/', {
            query: option,
            // preserveSnapshot: true
        })
        .map( photos => {
            return photos.map( photo => {
                // this.db.object("/users/"+photo.uid).subscribe( userObj => {
                //     photo['user'] = userObj.$value && userObj.$value === null ? {}:userObj;
                // })

                // this.db.object("/photosize/"+photo.photosize).subscribe( photosizeObj => {
                //     photo['photoType'] = photosizeObj.$value && photosizeObj.$value === null ? {}:JSON.stringify(photosizeObj);
                // })

                return photo;
            }).reverse();
        });
    }

    photoCount(photos): any {

        let total_photos = 0;
        photos.map(photo => {
            total_photos += photo.quantity
        });

        return total_photos;
    }

    scoreCount(photos): any {
        let score = 0;
        
        photos.map( photo => {
            score = score + photo.score;
        })

        return score;
    }

    salaryCount(photos): any {
        let salary = 0;
        photos.map( photo => {
            salary += photo.salary;
        })

        return salary;
    }

    photoSortByCategory(photos): any {
        let category = {};

        photos.map( photo => {
            if ( category[ photo.photosizeid ] ) {
                category[ photo.photosizeid ].quantity += photo.quantity;
            } else {
                category[ photo.photosizeid ] = {
                    quantity: photo.quantity,
                    sizeName: photo.photosize.sizeName 
                }
            }
        })

        return category;
    }

    createPhotoData(data) {
        return this.db.list('/photos').push(data);
    }

    updatePhotoData(key: string, data: any = {}) {
        return this.db.object('/photos/'+key).update(data);
    }

    generatePath(data) {
        let date = new Date(data);
        let path = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
        return path;
    }

    delete(key) {
        return this.db.object('/photos/'+key).remove();
    }
}
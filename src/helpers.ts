export const ROOTPAGE = "PhotosPage";

export function dateToNumber(date) {
    return date.valueOf();
}

export function todayToValue() {
    var today = new Date();
    return dateToNumber(today);
}